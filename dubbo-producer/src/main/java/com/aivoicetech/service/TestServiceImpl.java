package com.aivoicetech.service;

        import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author ssy
 * @date 2022/7/14 15:31
 */
@DubboService
public class TestServiceImpl implements TestService {

    public String getUserName() {
        return "testUser";
    }

}
