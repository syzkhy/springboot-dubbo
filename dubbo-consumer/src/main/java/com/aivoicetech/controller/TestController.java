package com.aivoicetech.controller;

import com.aivoicetech.service.TestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ssy
 * @date 2022/7/12 11:33
 */
@RestController
public class TestController {

    @Autowired
    public TestServiceImpl testService;

    @GetMapping("/test")
    public String test(){
        System.out.println("方法执行中。。。");
        return testService.getUserName();
    }

}
