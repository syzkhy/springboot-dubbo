package com.aivoicetech;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * dubbo消费者
 *
 */
@SpringBootApplication
@EnableDubbo
public class DubboConsumerApplication {

    public static void main( String[] args ) {
        System.setProperty("user.home","D:\\dubbo-file\\dubbo-consumer-file");
        SpringApplication.run(DubboConsumerApplication.class,args);
    }
}
