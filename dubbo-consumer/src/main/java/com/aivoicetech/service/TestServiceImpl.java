package com.aivoicetech.service;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

/**
 * @author ssy
 * @date 2022/7/14 15:31
 */
@Service
public class TestServiceImpl implements TestService {

    @DubboReference
    private TestService testService;

    public String getUserName() {
        return testService.getUserName();
    }

}
